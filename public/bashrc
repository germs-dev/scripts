#!/bin/bash

# To fetch this:
# curl -L turpin.cloud/bashrc

# To apply it locally:
# source <(curl -L turpin.cloud/bashrc)

# To apply it globally:
# [[ -e ~/.bash_aliases ]] && mv ~/.bash_aliases{,_}; cp <(curl -L turpin.cloud/bashrc) ~/.bash_aliases

# Git
alias gs="git status -s"
alias ga="git add"
alias gc="git commit -v"
alias gp="git push"
alias gb="git branch"
alias gd="git diff --color-words"
alias gl="git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'"

# Commit all and push
alias gx="git commit -va && git push"
alias gxm="git commit -am 'Minor change (reword)' && git push"

# Misc
export GIT_EDITOR=vim
alias wttr="curl wttr.in/brighton"
alias recursive_git_status='for dir in */.git; do cd $(dirname $dir); if [[ ! -z $(git status --short) ]]; then echo $dir; git status --short; fi; cd - >/dev/null; done'
alias wip="watch -d -n .5 ip -brief addr"

# Make sure we're running at CD quality
alias setclockrate="pw-metadata -n settings 0 clock.force-rate 44100"
setclockrate

# Things I do regularly but have not yet settled on an alias
# git pull
# sudo apt install --yes
# git stash --patch
# git commit --patch
# nmcli connection up <ap>

# Upgrade stuff
# sudo do-release-upgrade -d
alias fupgrade="sudo apt update && sudo apt full-upgrade --yes"

# Workaround for hang "debug1: rekey in after"
# OpenSSH_9.0p1 Ubuntu-1ubuntu7.1, OpenSSL 3.0.5 5 Jul 2022
export SSH_AUTH_SOCK=
