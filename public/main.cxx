#include "benchmark/benchmark.h"
#include "gtest/gtest.h"

// Entry point for both Google Test and Benchmark
int main(int argc, char **argv) {

  // Disable benchmarks by adding FILTER=blah to yourmake command line
  benchmark::Initialize(&argc, argv);
  benchmark::RunSpecifiedBenchmarks();

  int success = 0;

  ::testing::InitGoogleTest(&argc, argv);
  success = RUN_ALL_TESTS();

  return success;
}
