---
title:  Command line OpenAI
author: Dean Turpin
---

`openai` is an OpenAI API wrapper written in `bash`. It enables you to make OpenAI request on the command line; and additionally, it can preprocess any text file and replace queries with OpenAI responses. Let's start with a haiku written by a computer:

```bash
$ openai haiku about yoga but replace yoga with C++
{openai haiku about yoga but replace yoga with C++}
```

## Making queries (aka prompts)

You can make a simple query on the command line (see haiku example above). But the script can also update a text file passed as an argument.

```bash
openai README.md
```

To add a query to your file, simply wrap it in curly braces.

  {make up a joke about Harry Potter and a squirrel}

The script will expand it using the OpenAI response.

```bash
{make up a joke about Harry Potter and a squirrel}
```

## Build your own webpage using OpenAI queries

This page is built and hosted entirely within GitLab. If you edit in the GitLab web IDE you can created a website without leaving browser.

- Take a look at [the script](openai)
- Sign up to [OpenAI API](https://beta.openai.com/signup)
- Create API key in [preferences](https://platform.openai.com/account/api-keys)
- Fork [this repo](https://gitlab.com/germs-dev/scripts/)
- Update README.md with your questions: anything you want OpenAI to resolve you put in curly braces
- Commit your changes to run the pipe
- Check GitLab Pages to see your generated web page

## Running on the command line

After installation you can just run it on the command line, provided you have your API key set in your shell environment.

### Process command line query

```bash
$ openai say what is my name in hindi script
आपका नाम हिन्दी में क्या है?
```

### Take file on stdin, replace curly braces, write to stdout

```bash
openai < README.md
```

### Take file as an argument, replace curly braces, _overwrite_ input file

```bash
openai info.txt
```

## Install on your system

```bash
sudo curl -L turpin.cloud/openai --output /usr/bin/openai
sudo chmod a+x /usr/bin/openai
```

## Running without installation

You can also run it without installation by employing some `bash` sleight of hand.

```bash
bash <(curl --silent -L turpin.cloud/openai) info.txt
```

## GitLab

I use this script to update the documentation for this page. Simply use the commands above in your GitLab CI definition. See the config [for this repo](https://gitlab.com/germs-dev/scripts/-/blob/main/.gitlab-ci.yml).

```bash
pages:
  image: ubuntu:devel
  stage: deploy
  script:

    # Deps
    - apt update
    - apt install --yes curl

    # Run OpenAI and update README
    - bash <(curl --silent -L turpin.cloud/openai) README.md
    
    # Generate webpage
    - curl -L turpin.cloud/md2html | bash

  artifacts:
    paths: [public/]
```

## Errors

If you see grumbles about API keys, then login to OpenAI and [create one](https://platform.openai.com/account/api-keys). Export it in your `.bashrc` (or `.zshrc` or whatever.)

```json
{
    "error": {
        "message": "Incorrect API key provided: =sdfsd. You can find your API key at https://platform.openai.com/account/api-keys.",
        "type": "invalid_request_error",
        "param": null,
        "code": "invalid_api_key"
    }
}
```

Quite often the server just coughs with a popular model!

```json
{
  "error": {
    "message": "The server had an error while processing your request. Sorry about that!",
    "type": "server_error",
    "param": null,
    "code": null
  }
}
```

`curl` can also fail in various ways. 28 is common for time outs, 6 catches the case when the network is down. Note: `curl` doesn't really generate JSON errors like this (below), but I've used the same format so the error handling is common.

```json
{
  "error": {
    "message": "curl error",
    "type": "network_error",
    "param": 20,
    "code": 28
  }
}
```

---

## Additional scripts

Other scripts I run to configure new machines.

### bashrc updater

See [the script](bashrc). Used to update bash aliases on a new machine.

```bash
curl -L turpin.cloud/bashrc | bash
```

### Configure a new VM for dev

See [the script](ubuntu). Note it requires sudo. (Don't run things downloaded from the Internet... apart from this.)

```bash
curl -L turpin.cloud/ubuntu | sudo bash
```

### Generic makefile

See [the makefile](makefile). Used in all my C++ projects, includes a curated set of compiler flags and linker options.

Override flags on the command line:

```bash
# Google Benchmark test filter
FILTER=tokenise

# Append to LD flags
LDFLAGS=-lgtest_main

# Override compilter optimisation
OPT=-Ofast
```

```bash
make --makefile=<(curl --silent -L turpin.cloud/makefile) 
```

### Static website generator

See [the script](md2html) and the [CSS](style.css). Converts a `README.md` into a `public/index.html` with CSS styling. Uses `pandoc`.

```bash
curl -L turpin.cloud/md2html | bash
```
